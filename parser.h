#pragma once
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "lex.h"
//#include "Poliz.h"

using namespace std;

template < class T, int max_size >
class Stack
{
    T s [max_size];
    int top;
public:
    Stack () { top = 0; }
   	void reset () { top = 0; }
    void push ( T i );
    T pop ();
    bool is_empty () { return top == 0; }
    bool is_full  () { return top == max_size; }
};
 
template < class T, int max_size >
void Stack < T, max_size > :: push (T i)
{
  	if ( !is_full() )
    	s [top++] = i;
  	else
    	throw "Stack_is_full";
}	
 
template <class T, int max_size >
T Stack < T, max_size > :: pop ()
{
  	if ( !is_empty() )
    	return s[--top];
  	else
    	throw "stack_is_empty";
}

class Poliz
{
    Lex        * p;
    int          size;
    int          free;	//верхняя
public:
	Poliz (int max_size)
	{
		p = new Lex [size = max_size];
		free = 0;
	}
	~Poliz() { delete [] p; }
    void put_lex ( Lex l ) 
    {
		p [free] = l;
		free++;
	}
    void put_lex ( Lex l, int place) { p [place] = l; }
    void blank () { free++; }
    int get_free () { return free; }
    Lex & operator[] (int index)
    {
        if ( index > size )
            throw "POLIZ:out of array";
        else
            if ( index > free )
                throw "POLIZ:indefinite element of array";
            else
                return p[index];
    }
    void print ()
    {
        for ( int i = 0; i < free; ++i )
            cout << p[i];
    }
};

class Parser 
{
         Lex          curr_lex;
         type_of_lex  c_type;
         int          c_val;
         Scanner      scan;
         Stack < int, 100 > st_int;				//стек описания переменных
         Stack < type_of_lex, 100 >  st_lex;	//стек типа лексем
         void         P();
         void         D1();
         void         D();
         void         B();
         void         S();
         void         E();
         void         E1();
         void         T();
         void         F();
         void         dec ( type_of_lex type);
         void         check_id ();
         void         check_op ();
         void         check_not ();
         void         eq_type ();
         void         eq_bool ();
         void         check_id_in_read ();
         void		  placegoto() 
         			{
         				int top = TID.gettop();
         				int i, j;
         				for (i=1; i<top; i++) {
         					//cout << TID[i].get_value()<< ".";
         					if (TID[i].get_label()) {
         						for (j=0; j<TID[i].getcurplace(); j++) {
         							prog.put_lex (Lex(POLIZ_LABEL, TID[i].get_value()), TID[i].getplace(j));
         						}
         					}
         				}
					}
         void         gl ()						//считываем лексему
                      {
                      	//cout << c_type << " " ;				//отладка
                        curr_lex = scan.get_lex();
                        c_type = curr_lex.get_type();
                        c_val = curr_lex.get_value();
                      }
public:
        Poliz        prog;
        Parser (const char *program ) : scan (program),prog (1000) {}
        void analyze();
};
 
void Parser::analyze ()
{
  	gl();
  	P();
  	placegoto();
  	prog.print();	//   //
  	cout << endl << "Yes!!!" << endl;
}
 
void Parser::P ()						// <программа>
{
	//cout<< "P";
  	if (c_type == LEX_PROGRAM)
    	gl();
 	else
    	throw curr_lex;
	if (c_type == LEX_FLPAREN )			// {
		gl();
	else
    	throw curr_lex;

  	D1();									
  	B();

  	if (c_type == LEX_FRPAREN )			// }
		gl();
	else 
    	throw curr_lex;

  	if (c_type != LEX_FIN)
    	throw curr_lex;
	//cout << "p";
}
 
void Parser::D1 ()						// 〈описания〉
{
	//cout<< "D1";
    D();

    while (c_type == LEX_SEMICOLON)			//мен на ;
    {
      	gl();    
      	D();				// как закончить??
    }
    //cout << "d1";
}
 
void Parser::D ()					//〈описание〉
{
	//cout<< "D";
	type_of_lex type;
	int lexnum;
  	int flag=0;	
  	st_int.reset();			//стек


	if ((c_type == LEX_INT) || (c_type == LEX_BOOL)) {
		type = c_type;
		gl();
		if (c_type != LEX_ID)
		    throw curr_lex;
		else
		{
		    //st_int.push ( c_val );
		    //gl();
    	    if ( TID[c_val].get_declare() ) { 		//одно 
		    	throw "twice";
		  	}
		    else 
		    {
		    	TID[c_val].put_declare();
		    	TID[c_val].unput_label();	//это переменная а не меточка
		    	TID[c_val].put_type(type);
		    }
		    lexnum = c_val;
		    gl();
		    if (c_type == LEX_ASSIGN)
		    {
			    	//kkkkkkkkkk
		    	gl();
		    	if (c_type == LEX_MINUS) {
		    		flag = 1;
		    		gl();
		    	}
		    	if (c_type == LEX_NUM) {
		    		if (type == LEX_BOOL) {
		    			if ((c_val != 1)&&(c_val != 0)) {
		    				throw "wrong bool";
		    			}
		    		}
		    		TID[lexnum].put_assign();
		    		TID[lexnum].put_value((c_val)*(-1));
		    		gl();
		    	}
		    	//if (c_type == LEX_TRUE)&&(bool)			//оставим на потом!!!!!!!
		    }

		    while (c_type == LEX_COMMA)
		    {
		      gl();
		      if (c_type != LEX_ID)
		        throw curr_lex;
		      else
		      {
		        //st_int.push ( c_val );
		        //gl();
		        if ( TID[c_val].get_declare() ) { 		//одно 
		    		throw "twice";
			  	}
			    else 
			    {
			    	TID[c_val].put_declare();
			    	TID[c_val].unput_label();	//это переменная а не меточка
			    	TID[c_val].put_type(type);
			    }
			    //если = то
			    lexnum = c_val;
			    gl();
			    if (c_type == LEX_ASSIGN)
			    {
				    	//kkkkkkkkkk
			    	gl();
			    	if (c_type == LEX_NUM) {
			    		if (type == LEX_BOOL) {
			    			if ((c_val != 1)&&(c_val != 0)) {
			    				throw "wrong bool";
			    			}
			    		}
			    		TID[lexnum].put_assign();
			    		TID[lexnum].put_value(c_val);
			    		gl();
			    	}
			    	//if (c_type == LEX_TRUE)&&(bool)			//оставим на потом!!!!!!!
			    }
		      }
		    }							
		}  

		if (c_type != LEX_SEMICOLON)	// ;
	      throw curr_lex;

	      // НАДО ЗАКОММЕНТИТЬ ЭЛС!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	    /*
	    else
	    {
	      if (type == LEX_INT)
	      {
	        dec ( LEX_INT );		// это запись всего в табличку - надо перенести в др место
	        //gl();
	      }
	      else
	      if (type == LEX_BOOL)
	      {
	        dec ( LEX_BOOL );
	        //gl();
	      }
	      else throw curr_lex;
	    }
	    */ 
	}
	//else 
  		//throw curr_lex;

	//cout << "d";
    
}
 
void Parser::B ()					//〈операторы〉 ??
{
	//cout<< "B";
    S();
    while (c_type == LEX_SEMICOLON)
    {
      	gl();
      	S();
    }
    //cout << "b";
}
 
void Parser::S ()						//〈оператор〉
{
	//cout<< "S";
	int pl0, pl1, pl2, pl3;

	if (c_type == LEX_IF)
	{
		//cout << "o";
		gl();
		E();								
		
		eq_bool();							// последнее на стеке - булевское?  
		
		pl2 = prog.get_free ();				//запоминаем № верхней позиции в ПОЛИЗЕ
		prog.blank();						//оставляем пустое место
		prog.put_lex (Lex(POLIZ_FGO));
		//if (c_type == LEX_THEN)				// можно потом добавить { }
		//{
			S();
			gl();
			  //cout << c_type;
			if (c_type == LEX_ELSE)
			{
				pl3 = prog.get_free();
			  	prog.blank();
			  	prog.put_lex (Lex(POLIZ_GO));
			  	prog.put_lex (Lex(POLIZ_LABEL, prog.get_free()), pl2);			
			    gl();
			    S();
			    prog.put_lex (Lex(POLIZ_LABEL, prog.get_free()), pl3);
			}
			else
			{
			  	prog.put_lex (Lex(POLIZ_LABEL, prog.get_free()), pl2);

			}
		//}
		//else
		  //throw curr_lex;
	}//end if


/*
	if (c_type == LEX_IF)
	{
		gl();
		E();								//〈составной оператор〉 ??
		eq_bool();							// последнее на стеке - булевское?     тут
		pl2 = prog.get_free ();				//запоминаем № верхней позиции в ПОЛИЗЕ
		prog.blank();						//оставляем пустое место
		prog.put_lex (Lex(POLIZ_FGO));
		if (c_type == LEX_THEN)
		{
			  gl();			  
			  S();

			  pl3 = prog.get_free();
			  prog.blank();
			  prog.put_lex (Lex(POLIZ_GO));
			  prog.put_lex (Lex(POLIZ_LABEL, prog.get_free()), pl2);			
			  gl();										
			  if (c_type == LEX_ELSE)
			  {
			    gl();
			    S();
			    prog.put_lex (Lex(POLIZ_LABEL, prog.get_free()), pl3);
			  }
			  else
			    throw curr_lex;
		}
		else
		  throw curr_lex;
	}//end if
*/
	else if (c_type == LEX_WHILE)
	{
		pl0=prog.get_free();
		gl();
		E();
		eq_bool();
		pl1=prog.get_free(); prog.blank();
		prog.put_lex (Lex(POLIZ_FGO));
		//if (c_type == LEX_DO)
		//{
		  //gl();
		  S();
		  prog.put_lex (Lex (POLIZ_LABEL, pl0));
		  prog.put_lex (Lex ( POLIZ_GO));
		  prog.put_lex (Lex ( POLIZ_LABEL, prog.get_free()), pl1);
		//}
		//else
		//  throw curr_lex;
	}

	else if (c_type == LEX_READ)
	{
		gl();
		if (c_type == LEX_LPAREN)
		{
		  	gl();
			if (c_type == LEX_ID) {
				check_id_in_read();
				prog.put_lex (Lex ( POLIZ_ADDRESS, c_val) );
				gl();
			}
			else
			  	throw curr_lex;
			if ( c_type == LEX_RPAREN )
			{
			  	gl();
			  	prog.put_lex (Lex (LEX_READ));
			}
			else
			  	throw curr_lex;
		}
		else
			throw curr_lex;
	}

	else if (c_type == LEX_WRITE)
	{
		gl();
		if (c_type == LEX_LPAREN)
		{
		  gl();
		  E();
		  if (c_type == LEX_RPAREN)
		  {
		    gl();
		    prog.put_lex (Lex(LEX_WRITE));
		  }
		  else
		    throw curr_lex;
		}
		else
		  throw curr_lex;
	}

	else if (c_type == LEX_GOTO)
	{
		gl();
		if (c_type == LEX_ID) {
										
			TID[c_val].addplace(prog.get_free());	//		массив пустых мест, во внутрь которых в конце добавим адрес метки
			prog.blank();
			prog.put_lex (Lex(POLIZ_GO));
			gl();
		}
	}

	else if ( c_type == LEX_ID )
	{
		if ( TID[c_val].get_label() ) {		//если метка
			if (TID[c_val].get_assign()) {	//если уже такая метка есть
				throw ("redifinition of label");
			}
			TID[c_val].put_assign();		// "инициализировали" метку
			int val = c_val;
			gl();
			if (c_type == LEX_COLON) {		//ставим меточку
				TID[val].put_value(prog.get_free());		//значение метки - положение в ПОЛИЗЕ
				//надо gl??
				//cout << prog.get_free() << "!";
				c_type = LEX_SEMICOLON;
			}
			else
				throw curr_lex;

		}
		else {
			check_id ();
			prog.put_lex (Lex ( POLIZ_ADDRESS, c_val) );
			gl();
			if ( c_type == LEX_ASSIGN )
			{
			  gl();
			  E();
			  eq_type();								
			  prog.put_lex (Lex (LEX_ASSIGN) );
			}
			else {
			  throw curr_lex;
			}
		}
	}//assign-end
	else if (c_type == LEX_FLPAREN) {
		gl();
		S();

	    while (c_type == LEX_SEMICOLON)
	    {
	      gl();
	      if (c_type == LEX_FRPAREN){
	      	c_type = LEX_SEMICOLON;		//маскировочка
	      	break;
	      }
	      S();
	    }
	}
	//cout << "s";
}
 
void Parser::E () 					//〈оператор-выражение〉???
{
	//cout<< "E";
  	E1();
  	if ( c_type == LEX_EQ || c_type == LEX_LSS || c_type == LEX_GTR ||
       c_type == LEX_LEQ || c_type == LEX_GEQ || c_type == LEX_NEQ )
  	{
    	st_lex.push (c_type);
    	//cout << "&" << c_type << "&";
    	gl(); 
    	E1(); 
    	//cout << "cho";
    	check_op();
  	}
  //cout << "e";
}
 
void Parser::E1 ()
{
	//cout<< "E1";
  	T();
  	while ( c_type == LEX_PLUS || c_type == LEX_MINUS || c_type == LEX_OR)
  	{
    	st_lex.push (c_type);
    	//cout << "&" << c_type << "&";
    	gl();
    	T();
    	check_op();
  	}
  //cout << "e1";
}
 
void Parser::T ()
{
	//cout<< "T";
  	F();
  	while ( c_type == LEX_TIMES || c_type == LEX_SLASH || c_type == LEX_AND)
  	{
    	st_lex.push (c_type);
    //cout << "&" << c_type << "&";
   	gl();
   	F();
   	check_op();
  	}
  //cout << "t";
}
 
void Parser::F () 
{
	//cout<< "F";

	if ( c_type == LEX_MINUS) {
		gl();
		F();
		prog.put_lex (Lex (LEX_UNMINUS, 0));			// в ПОЛИЗ кидаем лексему унарный минус ??
	}

  	else if ( c_type == LEX_ID ) 
  	{
    	check_id();									// если переменная объявлена записываем в стек ее тип
    	prog.put_lex (Lex (LEX_ID, c_val));			// в ПОЛИЗ кидаем лексему
    	gl();
  	}
  	else if ( c_type == LEX_NUM ) 
  	{
    	st_lex.push ( LEX_INT );
    	prog.put_lex ( curr_lex );
    	gl();
  	}
  	else if ( c_type == LEX_TRUE ) 
  	{
    	st_lex.push ( LEX_BOOL );
    	prog.put_lex (Lex (LEX_TRUE, 1) );
    	gl();
  	}
  	else if ( c_type == LEX_FALSE)
  	{
    	st_lex.push ( LEX_BOOL );
    	prog.put_lex (Lex (LEX_FALSE, 0) );
    	gl();
  	}
  	else if (c_type == LEX_NOT) 
  	{
    	gl(); 
    	F(); 										
    	check_not();
  	}
  	else if ( c_type == LEX_LPAREN ) 
  	{
    	gl(); 
    	E();									//тут
    	if ( c_type == LEX_RPAREN)
      		gl();
    	else 
      		throw curr_lex;
  	}
  	else 
    	throw curr_lex;
	//cout << "f";
}

////////////////////////////////////////////////////////////////
 
void Parser::dec ( type_of_lex type ) 
{
  	int i;
  	while ( !st_int.is_empty()) 
  	{
    	i = st_int.pop();
    	if ( TID[i].get_declare() ) { 		//одно 
      		throw "twice";
  		}
    	else 
    	{
      		TID[i].put_declare();
      		TID[i].put_type(type);
    	}
  	}
}
 
void Parser::check_id () 						// если переменная объявлена
{												// записываем в стек ее тип
  	if ( TID[c_val].get_declare() )
    	st_lex.push ( TID[c_val].get_type() );
  	else 
    	throw "not declared";
}
 
void Parser::check_op () 			//разобраться
{
  	type_of_lex t1, t2, op, t;
 
 	//cout << "hi";

  	t2 = st_lex.pop(); //int
  	op = st_lex.pop(); //+
  	t1 = st_lex.pop(); //int
  
	//cout << "." << t2 << ".";
	//cout << "." << op << ".";
	//cout << "." << t1 << ".";


  	if (op == LEX_PLUS || op == LEX_MINUS || op == LEX_TIMES || op == LEX_SLASH)
    	t = LEX_INT;
  	if (op == LEX_OR || op == LEX_AND)
    	t = LEX_BOOL;
	if ( op == LEX_EQ || op == LEX_LSS || op == LEX_GTR || op == LEX_LEQ || op == LEX_GEQ || op == LEX_NEQ ) {
		//if ((t1 == t2)  &&  (t1 == LEX_INT)) {
			st_lex.push(LEX_BOOL);
		//}
		//else
    		//throw "wrong types are in operation";
	}
	else
  	if ((t1 == t2)  &&  (t1 == t))
    	st_lex.push(t);
  	else
    	throw "wrong types are in operation";
  	prog.put_lex (Lex (op) );
}
 
void Parser::check_not () 						// если булевская, то пишем LEX_NOT в ПОЛИЗ
{
  	if (st_lex.pop() != LEX_BOOL)
    	throw "wrong type is in not";
  	else 
  	{
    	st_lex.push (LEX_BOOL);
    	prog.put_lex (Lex (LEX_NOT));
  	}
}
 
void Parser::eq_type () 
{
	type_of_lex a, b;
	a = st_lex.pop();
	b = st_lex.pop();
	//cout << "!" << a << "!";
	//cout << "!"<< b << "!";

  	if (a!= b)
   		throw "wrong types are in :=";
}
 
void Parser::eq_bool () 
{
  	if ( st_lex.pop() != LEX_BOOL )
    	throw "expression is not boolean";
}
 
void Parser::check_id_in_read ()
{
  	if ( !TID [c_val].get_declare() )
    	throw "not declared";
}


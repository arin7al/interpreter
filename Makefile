CC = g++
CFLAGS = -g -Wall -std=c++0x
PROG = ui

all: $(PROG)

$(PROG): main.cpp executer.h parser.h lex.h
	$(CC) $(CFLAGS) $< -o $(PROG)

clean:
	rm -f *.o $(PROG)

run:
	rlwrap ./$(PROG)
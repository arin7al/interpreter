#pragma once
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

enum type_of_lex {
	LEX_NULL,
	LEX_PROGRAM, 
	LEX_VAR, 
	LEX_INT, 
	LEX_BOOL, 
	LEX_BEGIN, 
	LEX_END, 
	LEX_IF, 
	LEX_THEN, 
	LEX_ELSE,								//9 
	LEX_WHILE, 
	LEX_DO, 
	LEX_WRITE, 
	LEX_READ, 								//13
	LEX_OR, 
	LEX_AND, 
	LEX_NOT, 
	LEX_TRUE, 
	LEX_FALSE,
	LEX_FIN,	// @						//19
	LEX_SEMICOLON, //точка с запятой 		//20
	LEX_COMMA, //запятая
	LEX_COLON, //двоеточие
	LEX_ASSIGN, // присваивание =
	LEX_EQ,	//знак равно ==					//24
	LEX_NEQ,	// !=
	LEX_LSS,	// <
	LEX_GTR, // >
	LEX_PLUS, // +							//28
	LEX_MINUS, // -
	LEX_TIMES, // *
	LEX_SLASH,	// /
	LEX_LEQ, 	// <=
	LEX_GEQ,	// >=						//33
	LEX_LPAREN, 	// (
	LEX_RPAREN, 	// )
	LEX_FLPAREN,	// {
	LEX_FRPAREN,	// }					//37
	LEX_QUOT,		// "
	LEX_STRING,
	LEX_NUM,
	LEX_ID,									//41
	LEX_UNMINUS,							//42
	LEX_LABEL,	//метка	
	LEX_GOTO,	//оператор метки
	POLIZ_LABEL,
	POLIZ_ADDRESS,
	POLIZ_GO,					//47
	POLIZ_FGO
};

class Lex {
	type_of_lex t_lex;
	int v_lex; 
public:
	Lex ( type_of_lex t = LEX_NULL, int v = 0) {
		t_lex = t; 
		v_lex = v; 
	}
	type_of_lex get_type(){
		return t_lex; 
	}
	int get_value () {
		return v_lex;
	}
	friend std::ostream& operator << ( std::ostream &s, Lex l ) {
		s << '(' << l.t_lex << ',' << l.v_lex << ");";
		return s; 
	}
};

class Ident {
	char * name;
	bool declare; 
	type_of_lex type; 
	bool assign;
	bool label;
	int value;
	int places[100];
	int curplace;

public: 
	Ident (){ 
		declare = false;
		assign = false;
		label = true;
		curplace = 0;
	}	 
	char *get_name () {
		return name; 
	}
	void put_name (const char *n) {
		name = new char [ strlen(n) + 1 ];
		strcpy ( name, n ); 
	}
	bool get_declare () {
		return declare; 
	}
	void put_declare () {
		declare = true; 
	}
	type_of_lex get_type () {
		return type; 
	}
	void put_type ( type_of_lex t ) {
		type = t; 
	}
	bool get_assign () {
		return assign; 
	}
	void put_assign () {
		assign = true; 
	}
	void put_value (int v) {
		value = v; 
	}	
	void unput_label() {
		label = false;
	}
	bool get_label() {
		return label;
	}
	int get_value () {
		return value; 
	}
	void addplace(int a) {
		places[curplace++] = a;
	}
	int getcurplace() {
		return curplace;
	}
	int getplace(int i) {
		return places[i];
	}
};

class tabl_ident {
	Ident * p; 

	int size; 
	int top;
public:
	tabl_ident ( int max_size ) {
		p = new Ident[size=max_size];
		top = 1; 
	}
	~tabl_ident () {
		delete []p; 
	}
	Ident& operator[] ( int k ) {
		return p[k]; 
	}
	int put ( const char *buf );
	int gettop () {
		return top;
	}
};

/*
class Label {
	char * name;
public: 
	char *get_name () {
		return name; 
	}
	void put_name (const char *n) {
		name = new char [ strlen(n) + 1 ];
		strcpy ( name, n ); 
	}
};

class tabl_label {
	Label *p;
	int size;
	int top;
public:
	tabl_label ( int max_size ) {
		p = new Label[size=max_size];
		top = 1; 
	}
	~tabl_label () {
		delete []p; 
	}
	Label& operator[] ( int k ) {
		return p[k]; 
	}	
	int put ( const char *buf );
};
*/

int tabl_ident::put ( const char *buf ) {
	for ( int j=1; j<top; ++j )
		if ( !strcmp(buf, p[j].get_name()) )	//если такое имя уже инициализировано
			return j; 
	p[top].put_name(buf);
	++top;
	return top-1; 
}
/*
int tabl_label::put ( const char *buf ) {
	for ( int j=1; j<top; ++j )
		if ( !strcmp(buf, p[j].get_name()) )	//если такое имя уже инициализировано
			throw "Такая метка уже есть";
	p[top].put_name(buf);
	++top;
	return top-1; 
}
*/

class Scanner {
	enum state { H, IDENT, NUMB, COM, ALE, DELIM, NEQ };
	static char * TW[];
	static type_of_lex words[];
	static char * TD[];
	static type_of_lex dlms[];
	state CS;
	FILE *fp;
	char c;
	char buf[80];
	int buf_top;
	void clear ()
	{
		buf_top = 0;
		for ( int j = 0; j < 80; ++j )
			buf[j] = '\0';
	}	
	void add ()	
	{
		buf[ buf_top ++ ] = c;
	}
	int look ( const char *buf, char **list ) 
	{
		int i = 0;
		//cout << buf;
		while ( list[i] ) 
		{
			if ( strcmp(buf, list[i]) == 0) 
				return i;
			++i; 
		}
		return 0; 
	}
	void gc () {
		c = fgetc (fp); 
	}
	public:
		Lex get_lex ();
		void getce() {
			std::cout << c;
		}
	Scanner ( const char * program ) 
	{
		fp = fopen ( program, "r" ); 
		CS = H;
		clear();
		gc();
	}
};

char * Scanner::TW[] =
{
	(char*)"", // 0 позиция 0 не используется
	(char*)"and", //1 
	(char*)"begin", //2 
	(char*)"bool", //3
	(char*)"do", //4 
	(char*)"else", //5 
	(char*)"end", //6 
	(char*)"if", //7 
	(char*)"false", //8 
	(char*)"int", //9 
	(char*)"not", // 10 
	(char*)"or", // 11 
	(char*)"program", // 12 
	(char*)"read", // 13 
	(char*)"then", // 14 
	(char*)"true", // 15 
	(char*)"var", // 16 
	(char*)"while", // 17 
	(char*)"write", // 18
	(char*)"string", //19
	(char*)"goto",	//20
	NULL 
};
char * Scanner:: TD[] = {
	(char*)"", // 0 позиция 0 не используется
	(char*)"@", //1 
	(char*)";", //2 
	(char*)",", //3 
	(char*)":", //4 
	(char*)"==", //5 
	(char*)"(", //6 
	(char*)")", //7 
	(char*)"=", //8 
	(char*)"<", //9 
	(char*)">", // 10 
	(char*)"+", // 11 
	(char*)"-", // 12 
	(char*)"*", // 13 
	(char*)"/", // 14
	(char*)"<=", // 15
	(char*)"!=", // 16 
	(char*)">=", // 17
	(char*)"{", //18
	(char*)"}", //19	
	(char*)"\"", //20
	NULL 
};

tabl_ident TID(300);
//tabl_label TL(100);

type_of_lex Scanner::words[] = {
	LEX_NULL, 
	LEX_AND, 
	LEX_BEGIN, 
	LEX_BOOL, 
	LEX_DO, 
	LEX_ELSE, 
	LEX_END, 
	LEX_IF, 
	LEX_FALSE, 
	LEX_INT, 
	LEX_NOT, 
	LEX_OR, 
	LEX_PROGRAM, 
	LEX_READ, 
	LEX_THEN, 
	LEX_TRUE, 
	LEX_VAR,
	LEX_WHILE, 
	LEX_WRITE, 
	LEX_STRING,
	LEX_GOTO,
	LEX_NULL
};

type_of_lex Scanner::dlms[] = 
{
	LEX_NULL, 
	LEX_FIN, 
	LEX_SEMICOLON, 
	LEX_COMMA, 
	LEX_COLON, 
	LEX_EQ, 
	LEX_LPAREN,							//???????
	LEX_RPAREN, //скобки	
	LEX_ASSIGN, 
	LEX_LSS, 
	LEX_GTR, 
	LEX_PLUS, 
	LEX_MINUS, 
	LEX_TIMES, 
	LEX_SLASH, 
	LEX_LEQ, 	
	LEX_NEQ, 	
	LEX_GEQ, 
	LEX_FLPAREN, 	// {
	LEX_FRPAREN, 	// }
	LEX_QUOT,		// "
	LEX_NULL,

};

Lex Scanner::get_lex () {
	int d, j;
	CS = H;
	do {
		switch (CS ) {
		case H:		
			if ( c ==' ' || c =='\n' || c=='\r' || c =='\t' ) {
				//cout << " probelii  ";
				gc ();	
			}			
			else if (isalpha(c)) {
				clear (); 
				add ();
				gc ();
				CS = IDENT;
			}
			else if (isdigit(c)) {
				d = c - '0'; 
				gc ();
				CS = NUMB;
			}

			else if ( c == '=' || c== '<' || c== '>') {
				clear (); 
				add (); 
				gc (); 
				CS = ALE;
			}
			else if ( c == '@' )
				return Lex(LEX_FIN);
			else if ( c == '!' ) {
				clear (); 
				add (); 
				gc ();
				CS = NEQ;
			} 
			else if ( c == '/' ) {
				clear (); 
				add (); 
				gc (); 
				if ( c == '*') {	//идем в коммент
					CS = COM;
				}
				else {
					j = look(buf, TD);	//добавляем слэш
					return Lex ( dlms[j], j);
				}
			}
			else {
				//cout << " delim ";
				CS = DELIM; 	
			}
			
			break;

		case IDENT:
			if (isalpha(c) || isdigit(c)){
				add ();
				gc (); 
			}
			else {
				/*
				if (c == ':') {
					gc();
					j = TID.put(buf); 	//добавляем метку в таблицу идентификаторов
					TID[j].put_label();
					return Lex (LEX_LABEL, j);
				}
				*/
				if (look (buf, TW)) {
					j = look (buf, TW);
					//cout << " tw ";
					return Lex (words[j], j); // сама лексема + номер в таблице TW
				}
				else {
					//cout << " hz ";
					j = TID.put(buf); 
					return Lex (LEX_ID, j);
				} 
			}
			break;

		case NUMB:
			if (isdigit(c)){
				d=d*10+(c-'0'); 
				gc();
			} 
			else
				return Lex ( LEX_NUM, d ); 
			break;

		case COM:
			if (c == '*'){
				gc ();				
				if (c == '/') {
					gc ();
					CS = H; 
				}			
			}
			else if (c == '@' || c == '{' ) 
				throw c;
			else
				gc ();
			break; 

		case ALE:						// == = >= <=
			if ( c == '=' ) {
				add ();
				gc ();
				j = look ( buf, TD ); 
				return Lex ( dlms[j], j );
			}
			else {
				j = look (buf, TD);
				return Lex ( dlms[j], j );
			}
			break; 

		case NEQ:
			if ( c == '=' ) {
				add (); 
				gc ();
				j = look(buf, TD);
				return Lex( LEX_NEQ, j);
			}
			else
				throw '!'; 
			break;

		case DELIM: 	
			//cout << " hd ";
			clear ();
			add ();
			if ( look(buf, TD) ) {
				j = look(buf, TD);
				gc ();

				return Lex ( dlms[j], j);
			} 
			else
				throw c;					
			break;
		} // end switch }
	}
	while ( true ); 
}


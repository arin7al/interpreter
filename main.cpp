#include <iostream>
#include <fstream>
#include <string.h>
#include "executer.h"

using namespace std;

//extern TID;

int main() {
	try {
		//Parser pars("program2.txt");
		//pars.analyze();
		Interpretator I ((char*)"program1.txt");
    	I.interpretation ();
    	return 0;
	}
	catch (char c) {
		cout << "unexpected symbol " << c << endl;
    	return 1;
	}
	catch (Lex l) {
		cout << "unexpected lexeme";
    	cout << l;
    	return 1;
	}
	catch (char const* a) {
		cout << a << endl;
		return 1;
	}
}
